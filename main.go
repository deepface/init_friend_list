package main

import (
	"context"
	"fmt"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/chromedp"
	"log"
	"os"
	"regexp"
	"strconv"
	"time"
)

const (
	loginURL = "https://vk.com"
)

func main() {
	var (
		login string
		pass  string
	)

	args := os.Args[1:]
	for i, arg := range args {
		if arg == `--vk-login` {
			login = args[i+1]
		}

		if arg == `--vk-pass` {
			pass = args[i+1]
		}
	}

	if len(login) == 0 || len(pass) == 0 {
		fmt.Println("Usage: ./init_friend_list --vk-login foo --vk-pass qwerty")
		os.Exit(1)
	}

	opts := append(chromedp.DefaultExecAllocatorOptions[:],
		chromedp.Flag("headless", false),
	)

	actx, cancel := chromedp.NewExecAllocator(context.Background(), opts...)

	ctx, cancel := chromedp.NewContext(actx, chromedp.WithLogf(log.Printf))

	ctx, cancel = context.WithTimeout(ctx, 15*time.Minute)
	defer cancel()

	var (
		friends      []*cdp.Node
		friendsCount string
	)

	err := chromedp.Run(ctx,
		chromedp.Navigate(loginURL),
		chromedp.WaitReady("button#index_login_button"),
		chromedp.SendKeys("input#index_email", login),
		chromedp.SendKeys("input#index_pass", pass),
		chromedp.Click("button#index_login_button"),
		// Go to friend list
		chromedp.WaitVisible("li#l_fr"),
		chromedp.Click("li#l_fr > a.left_row"),

		chromedp.Text("#friends_tab_all > a.ui_tab > span.ui_tab_count", &friendsCount),
		chromedp.WaitVisible("//div[@class='friends_list_bl']"),
	)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(friendsCount)

	loadedFriendNodes := 0
	friendNodesTotal, _ := strconv.Atoi(friendsCount)

	// Скроллим вниз, пока не загрузится весь список друзей
	for loadedFriendNodes < friendNodesTotal {
		if err = chromedp.Run(ctx,
			chromedp.ScrollIntoView("//div[@class='friends_list_bl'][last()]"),
		); err != nil {
			log.Fatal(err)
		}

		if err = chromedp.Run(ctx, chromedp.Nodes("div.friends_user_row", &friends)); err != nil {
			log.Fatal(err)
		}
		loadedFriendNodes = len(friends)

		log.Println("Loaded: ", loadedFriendNodes, " friends")
	}

	resultsFile, err := os.OpenFile("friends.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	userIDPattern := regexp.MustCompile(`^[a-z_]+(\d+)$`)
	for i := 0; i < len(friends); i++ {
		friendID := userIDPattern.ReplaceAllString(friends[i].AttributeValue("id"), "$1")

		row := fmt.Sprintf("%s\n", friendID)
		if _, err := resultsFile.Write([]byte(row)); err != nil {
			log.Fatal(err)
		}
	}

	if err := resultsFile.Close(); err != nil {
		log.Fatal(err)
	}
}
