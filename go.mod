module gitlab.com/deepface/init_friend_list

go 1.13

require (
	github.com/chromedp/cdproto v0.0.0-20191114225735-6626966fbae4
	github.com/chromedp/chromedp v0.5.2
)
